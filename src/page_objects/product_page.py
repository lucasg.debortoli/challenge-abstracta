from src.page_objects.base_page import BasePage
from src.locators.product_page_locators import ProductPageLocators


class ProductPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def add_to_cart(self):
        self.find_element(ProductPageLocators.add_to_cart).click()

        # Wait until element is added to cart
        self.find_element(ProductPageLocators.success_message_added, wait=3)
