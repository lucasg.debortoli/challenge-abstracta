from selenium.webdriver.common.by import By


class ProductPageLocators:
    add_to_cart = By.ID, "button-cart"
    success_message_added = By.XPATH, "//div[contains(@class, 'alert-success')]"
