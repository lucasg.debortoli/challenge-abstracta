from selenium.webdriver.common.by import By


class SearchPageLocators:
    first_product_name = By.XPATH, ("//div[@id=\"content\"]/div[@class=\"row\"][3]"
                                    "/div[starts-with(@class, \"product-layout\")][1]//h4/a")
