from src.page_objects.base_page import BasePage
from src.locators.main_page_locators import MainPageLocators


class MainPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def search_product(self, search_term):
        parsed_search = search_term.rstrip() + "\n"
        self.find_element(MainPageLocators.search_bar).send_keys(parsed_search)
