import pytest

from src.tests.base_test import BaseTest
from src.page_objects.main_page import MainPage
from src.page_objects.search_page import SearchPage
from src.page_objects.product_page import ProductPage
from src.page_objects.cart_modal_page import CartModalPage
from src.page_objects.shopping_cart_page import ShoppingCartPage


@pytest.fixture
def main_page(driver):
    main_page = MainPage(driver)
    yield main_page


@pytest.fixture
def search_page(driver):
    search_page = SearchPage(driver)
    yield search_page


@pytest.fixture
def cart_modal_page(driver):
    cart_modal_page = CartModalPage(driver)
    yield cart_modal_page


@pytest.fixture
def product_page(driver):
    cart_modal_page = ProductPage(driver)
    yield cart_modal_page


@pytest.fixture
def shopping_cart_page(driver):
    shopping_cart_page = ShoppingCartPage(driver)
    yield shopping_cart_page


@pytest.mark.ui
@pytest.mark.usefixtures("main_page", "search_page", "cart_modal_page", "product_page", "shopping_cart_page")
class TestIphoneShop(BaseTest):

    def test_search_iphone(self, main_page, search_page, cart_modal_page, product_page, shopping_cart_page):
        main_page.search_product("iPhone")
        product_id = search_page.select_first_product()
        product_page.add_to_cart()
        cart_modal_page.go_to_cart()
        assert shopping_cart_page.find_product(product_id) is not None
        shopping_cart_page.remove_product(product_id)
        assert shopping_cart_page.product_is_not_in_cart(product_id)
