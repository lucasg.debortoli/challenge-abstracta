from src.page_objects.base_page import BasePage
from src.locators.search_page_locators import SearchPageLocators
from src.utils.helpers import parse_product_id


class SearchPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def select_first_product(self):
        product_name = self.find_element(SearchPageLocators.first_product_name)
        href = product_name.get_attribute("href")
        product_name.click()
        return parse_product_id(href)
