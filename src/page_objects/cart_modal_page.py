from src.page_objects.base_page import BasePage
from src.locators.cart_modal_page_locators import CartModalPageLocators


class CartModalPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def go_to_cart(self):
        self.find_element(CartModalPageLocators.cart_button).click()
        self.find_element(CartModalPageLocators.view_cart, wait=3).click()
