from selenium.webdriver.common.by import By


class ShoppingCartPageLocators:
    def product(product_id):
        return By.XPATH, ("//div[@class=\"table-responsive\"]/table/tbody/tr[1]/td[2]/"
                          f"a[contains(@href, 'product_id={product_id}')]")

    def remove_product(product_id):
        return By.XPATH, ("//div[@class=\"table-responsive\"]/table/tbody/tr[1]/td[2]/"
                          f"a[contains(@href, 'product_id={product_id}')]"
                          "/parent::td/parent::tr/td[4]//button[contains(@class, 'btn-danger')]")
