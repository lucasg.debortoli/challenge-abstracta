from src.page_objects.base_page import BasePage
from src.locators.shopping_cart_page_locators import ShoppingCartPageLocators


class ShoppingCartPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def find_product(self, product_id):
        return self.find_element(ShoppingCartPageLocators.product(product_id), wait=3)

    def remove_product(self, product_id):
        return self.find_element(ShoppingCartPageLocators.remove_product(product_id)).click()

    def product_is_not_in_cart(self, product_id, wait=5):
        not_present = self.wait_until_element_not_present(ShoppingCartPageLocators.product(product_id), wait)
        self.take_screenshot("is_iphone_not_present.png")
        return not_present is not None