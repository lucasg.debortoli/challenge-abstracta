from selenium.webdriver.common.by import By


class CartModalPageLocators:
    cart_button = By.ID, "cart"
    view_cart = By.XPATH, "//strong[normalize-space(text())=\"View Cart\"]/parent::a"
